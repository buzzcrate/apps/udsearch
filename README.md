# UDSearch

A python Discord bot to perfom Urban Dictionary lookups.


## Use

 
 UDSearch commands:

`/udlookup <searchitem>` -Returns the full list of definitions for <searchitem>.  Alias:/UDL

`/udtop <searchitem>` -Returns definition with highest net thumbs up to down count.  Alias:/UDT

`/udrandom <searchitem>` -Returns a random definition for <searchitem>.  Alias:/UDR
